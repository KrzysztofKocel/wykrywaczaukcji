package pl.auctiondetector.rss.model;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

import java.util.regex.Pattern;

import static com.google.common.base.Joiner.on;
import static com.google.common.collect.FluentIterable.from;

public class DescriptionConverter implements Converter<Description> {

    private static final Pattern BR_PATTERN = Pattern.compile("<br\\s*/>", Pattern.CASE_INSENSITIVE);
    private static final Splitter LINE_SPLITTER = Splitter.on(BR_PATTERN);

    @Override
    public Description read(InputNode node) throws Exception {

        Description description = new Description();

        ImmutableList<String> lines = ImmutableList.copyOf(LINE_SPLITTER.split(node.getValue()));

        HrefAndAnchor buyer = extractHrefAndAnchor(cleanInputFromHeaders(lines.get(0)));
        description.buyerLink = buyer.getHref();
        description.buyerName = buyer.getAnchor();
        description.price = cleanInputFromHeaders(lines.get(1));
        description.endDate = cleanInputFromHeaders(lines.get(2));
        description.itemLink = extractHrefAndAnchor(lines.get(3)).getHref();
        description.imageLink = getImageSource(lines.get(5));

        return description;
    }

    @Nullable
    private Uri getImageSource(String entry) {
        FluentIterable<ImageSpan> spans = getSpans(Html.fromHtml(entry), ImageSpan.class);

        if (!spans.isEmpty()) {
            Uri uri = Uri.parse(spans.first().get().getSource());

            FluentIterable<String> path = from(uri.getPathSegments());
            String newPath = path.limit(1).append("oryginal").append(path.skip(2)).join(on("/"));

            return uri.buildUpon().path(newPath).build();

        } else {
            return null;
        }
    }

    private String cleanInputFromHeaders(String input) {
        return Iterables.getLast(Splitter.on(": ").trimResults().split(input));
    }

    private HrefAndAnchor extractHrefAndAnchor(String input) {
        FluentIterable<URLSpan> spans = getSpans(Html.fromHtml(input), URLSpan.class);

        if (!spans.isEmpty()) {
            return new HrefAndAnchor(spans.first().get().getURL(), input.trim());
        } else {
            return new HrefAndAnchor(null, input.trim());
        }
    }

    private <T> FluentIterable<T> getSpans(Spanned spanned, Class<T> spanClass) {
        return FluentIterable.of(spanned.getSpans(0, spanned.length(), spanClass));
    }

    @Override
    public void write(OutputNode node, Description value) throws Exception {

    }

    private static class HrefAndAnchor {

        @Nullable
        private final Uri mHref;
        @NonNull
        private final String mAnchor;

        public HrefAndAnchor(String href, @NonNull String anchor) {

            if (!Strings.isNullOrEmpty(href)) {
                mHref = Uri.parse(href);
            } else {
                mHref = null;
            }

            mAnchor = Preconditions.checkNotNull(anchor);
        }

        @Nullable
        public Uri getHref() {
            return mHref;
        }

        @NonNull
        public String getAnchor() {
            return mAnchor;
        }

    }
}
