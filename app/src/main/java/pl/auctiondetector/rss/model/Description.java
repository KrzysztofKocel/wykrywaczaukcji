package pl.auctiondetector.rss.model;

import android.net.Uri;

public class Description {
    String buyerName;
    Uri buyerLink;
    String endDate;
    String price;
    Uri itemLink;
    Uri imageLink;

}
