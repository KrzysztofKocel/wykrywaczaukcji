package pl.auctiondetector.rss.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;

@Root(name = "item", strict = false)
public class Item {
    @Element
    String title;

    @Element
    String link;

    @Element
    @Convert(DescriptionConverter.class)
    Description description;

    @Element
    String pubDate;
}