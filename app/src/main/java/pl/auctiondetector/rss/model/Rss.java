package pl.auctiondetector.rss.model;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.Collection;
import java.util.List;

import pl.auctiondetector.model.Auction;

import static pl.auctiondetector.model.Auction.Builder.auction;

@Root(name="rss", strict = false)
public class Rss {
    @ElementList(entry="channel", inline = true)
    List<Channel> channels;

    public Collection<Auction> getAuctions(){

        return FluentIterable.from(channels.get(0).items).transform(new Function<Item, Auction>() {
            @Override
            public Auction apply(Item input) {

                return auction()
                        .withAuctionImage(input.description.imageLink)
                        .withAuctionLink(input.description.itemLink)
                        .withAuctionTitle(input.title)
                        .withBuyerName(input.description.buyerName)
                        .withBuyerLink(input.description.buyerLink)
                        .withPrice(input.description.price)
                        .withEndDate(input.description.endDate)
                        .build();
            }
        }).toList();

    }
}
