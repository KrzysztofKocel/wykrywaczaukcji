package pl.auctiondetector.rss.service;

import java.util.Map;

import pl.auctiondetector.rss.model.Rss;
import retrofit.http.GET;
import retrofit.http.QueryMap;

public interface RssService {
    String URL = "http://allegro.pl";

    public interface Query{
        String STRING = "string";
        String CATEGORY = "category";
        String STATE = "state";
        String SELECTED_COUNTRY = "selected_country";
        String SEARCH_TYPE = "search_type";
        String OFFER_TYPE = "offer_type";
        String PRICE_FROM = "price_from";
        String PRICE_TO = "price_to";
        String CITY = "city";
        String POSTCODE_ENABLED = "postcode_enabled";

    }

    @GET("/rss.php/search")
    Rss getAuctionsRss(@QueryMap Map<String, String> options);
}