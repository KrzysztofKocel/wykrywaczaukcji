package pl.auctiondetector;

import dagger.Module;
import dagger.Provides;
import pl.auctiondetector.ui.BaseActivity;

@Module
public class ActivityModule {
    final BaseActivity mActivity;

    public ActivityModule(BaseActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides @PerActivity BaseActivity activity() {
        return mActivity;
    }
}
