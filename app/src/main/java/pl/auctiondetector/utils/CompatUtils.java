package pl.auctiondetector.utils;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

public class CompatUtils {

    public static void setViewBackground(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setViewBackgroundV16(view, drawable);
        } else {
            setViewBackgroundLegacy(view, drawable);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static void setViewBackgroundV16(View view, Drawable drawable) {
        view.setBackground(drawable);
    }

    @SuppressWarnings("deprecation")
    private static void setViewBackgroundLegacy(View view, Drawable drawable) {
        view.setBackgroundDrawable(drawable);
    }
}
