package pl.auctiondetector;

public interface HasComponent<C> {
    C getComponent();
}
