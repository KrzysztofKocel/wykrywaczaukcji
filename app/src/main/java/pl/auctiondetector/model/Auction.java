package pl.auctiondetector.model;

import android.net.Uri;

public class Auction {

    private String mBuyerTitle;
    private Uri mBuyerLink;

    private String mTitle;
    private String mPrice;

    private Uri mAuctionLink;
    private Uri mAuctionImage;

    private String mEndDate;

    public String getBuyerTitle() {
        return mBuyerTitle;
    }

    public Uri getBuyerLink() {
        return mBuyerLink;
    }

    public String getPrice() {
        return mPrice;
    }

    public Uri getAuctionLink() {
        return mAuctionLink;
    }

    public Uri getAuctionImage() {
        return mAuctionImage;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public static class Builder {
        private String mBuyerTitle;
        private Uri mBuyerLink;
        private String mPrice;
        private Uri mAuctionLink;
        private Uri mAuctionImage;
        private String mTitle;

        private String mEndDate;

        private Builder() {
        }

        public static Builder auction() {
            return new Builder();
        }

        public Builder withBuyerName(String buyerTitle) {
            mBuyerTitle = buyerTitle;
            return this;
        }

        public Builder withBuyerLink(Uri buyerLink) {
            mBuyerLink = buyerLink;
            return this;
        }

        public Builder withPrice(String price) {
            mPrice = price;
            return this;
        }

        public Builder withAuctionLink(Uri auctionLink) {
            mAuctionLink = auctionLink;
            return this;
        }

        public Builder withAuctionImage(Uri auctionImage) {
            mAuctionImage = auctionImage;
            return this;
        }

        public Builder withAuctionTitle(String auctionTitle) {
            mTitle = auctionTitle;
            return this;
        }

        public Builder withEndDate(String endDate) {
            mEndDate = endDate;
            return this;
        }


        public Auction build() {
            Auction auction =  new Auction();
            auction.mAuctionImage = mAuctionImage;
            auction.mAuctionLink = mAuctionLink;
            auction.mBuyerLink = mBuyerLink;
            auction.mBuyerTitle = mBuyerTitle;
            auction.mPrice = mPrice;
            auction.mTitle = mTitle;
            auction.mEndDate = mEndDate;

            return auction;
        }
    }
}
