package pl.auctiondetector.ui.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import pl.auctiondetector.HasComponent;
import pl.auctiondetector.R;
import pl.auctiondetector.ui.BaseFragment;

public class AuctionDetailsFragment extends BaseFragment {

    interface Injector {
        AuctionDetailsFragment inject(AuctionDetailsFragment fragment);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        safeInjectFragment(getActivity(), this);
    }

    private void safeInjectFragment(FragmentActivity activity, AuctionDetailsFragment fragment) {
        if (activity instanceof HasComponent) {
            Object component = ((HasComponent) activity).getComponent();
            if (component instanceof Injector) {
                ((Injector)component).inject(fragment);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_auction_details, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        ButterKnife.reset(this);
        super.onDestroyView();
    }
}
