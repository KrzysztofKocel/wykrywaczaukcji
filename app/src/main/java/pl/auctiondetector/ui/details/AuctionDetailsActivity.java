package pl.auctiondetector.ui.details;

import android.os.Bundle;

import pl.auctiondetector.ActivityModule;
import pl.auctiondetector.App;
import pl.auctiondetector.HasComponent;
import pl.auctiondetector.R;
import pl.auctiondetector.ui.BaseActivity;

public class AuctionDetailsActivity extends BaseActivity implements HasComponent<AuctionDetailsComponent> {

    private AuctionDetailsComponent mComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mComponent = Dagger_AuctionDetailsComponent.builder()
                .appComponent(App.from(this).component())
                .activityModule(new ActivityModule(this))
                .build();

        mComponent.injectActivity(this);

        setContentView(R.layout.activity_auction_details);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new AuctionDetailsFragment())
                    .commit();
        }
    }

    @Override
    public AuctionDetailsComponent getComponent() {
        return mComponent;
    }
}
