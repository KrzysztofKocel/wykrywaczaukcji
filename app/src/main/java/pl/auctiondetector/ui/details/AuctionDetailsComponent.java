package pl.auctiondetector.ui.details;

import dagger.Component;
import pl.auctiondetector.ActivityModule;
import pl.auctiondetector.AppComponent;
import pl.auctiondetector.PerActivity;

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface AuctionDetailsComponent extends AuctionDetailsFragment.Injector {

    AuctionDetailsActivity injectActivity(AuctionDetailsActivity activity);

}
