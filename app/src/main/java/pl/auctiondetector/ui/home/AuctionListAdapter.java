package pl.auctiondetector.ui.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.Collection;

import pl.auctiondetector.BuildConfig;
import pl.auctiondetector.R;
import pl.auctiondetector.model.Auction;
import pl.auctiondetector.ui.home.palette.BoxResizerTransformer;
import pl.auctiondetector.ui.home.palette.PaletteTransformation;

public class AuctionListAdapter extends RecyclerView.Adapter<AuctionListAdapter.ViewHolder> {

    private volatile Picasso mPicasso;
    private ImmutableList<Auction> mData = ImmutableList.of();

    public void setData(Collection<Auction> data) {
        mData = ImmutableList.copyOf(data);
        notifyDataSetChanged();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ImageView mImageView;

        // each data item is just a string in this case
        public ViewHolder(View view) {
            super(view);

            mTextView = (TextView) view.findViewById(R.id.auction_name);
            mImageView = (ImageView) view.findViewById(R.id.auction_image);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AuctionListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_auction, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Auction auction = mData.get(position);
        holder.mTextView.setText(auction.getTitle());

        Optional<Uri> auctionImageLink = Optional.fromNullable(auction.getAuctionImage());

        if (auctionImageLink.isPresent()) {

            getPicassoRequestCreator(auctionImageLink.get(), holder.mImageView)
                    .transform(PaletteTransformation.instance())
                    .into(holder.mImageView, new Callback.EmptyCallback() {
                        @Override
                        public void onSuccess() {
                            Bitmap bitmap = ((BitmapDrawable) holder.mImageView.getDrawable()).getBitmap();
                            Palette palette = PaletteTransformation.getPalette(bitmap);
                            Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
                            if (holder.mTextView != null && vibrantSwatch != null) {
                                setTitleBackground(vibrantSwatch, holder);
                                holder.mTextView.setTextColor(vibrantSwatch.getTitleTextColor());
                            }
                        }
                    });
        }
    }

    protected void setTitleBackground(Palette.Swatch vibrantSwatch, ViewHolder holder) {
        holder.mTextView.setBackgroundColor(vibrantSwatch.getRgb() & 0xefffffff);
    }

    protected RequestCreator getPicassoRequestCreator(Uri auctionImageLink, ImageView imageView) {
        return getPicasso(imageView.getContext())
                        .load(auctionImageLink)
                        .fit()
                        .centerCrop();
    }

    protected Picasso getPicasso(Context context) {
        if (mPicasso == null) {
            synchronized (this) {
                if (mPicasso == null) {
                    mPicasso = new Picasso.Builder(context)
                            .requestTransformer(BoxResizerTransformer.instance())
                            .build();

                    mPicasso.setIndicatorsEnabled(BuildConfig.DEBUG);
                    mPicasso.setLoggingEnabled(BuildConfig.DEBUG);
                }
            }
        }

        return mPicasso;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mData.size();
    }

}