package pl.auctiondetector.ui.home.palette;

import android.net.Uri;

import com.squareup.picasso.Picasso.RequestTransformer;
import com.squareup.picasso.Request;

public class BoxResizerTransformer implements RequestTransformer {

    private static final BoxResizerTransformer INSTANCE = new BoxResizerTransformer();

    public static BoxResizerTransformer instance() {
        return INSTANCE;
    }

    @Override
    public Request transformRequest(Request request) {
        if (request.resourceId != 0) {
            return request; // Don't transform resource requests.
        }

        Uri targetUri = request.uri;
        String scheme = targetUri.getScheme();

        if (!"https".equals(scheme) && !"http".equals(scheme)) {
            return request; // Support remote images.
        }

        Request.Builder builder = request.buildUpon();

        int targetHeight = request.targetHeight;
        int targetWidth = request.targetWidth;

        builder.setUri(
                new Uri.Builder()
                        .scheme("http")
                        .authority("proxy.boxresizer.com")
                        .path("convert")
                        .appendQueryParameter("source", targetUri.toString())
                        .appendQueryParameter("resize", targetWidth + "x" + targetHeight)
                        .appendQueryParameter("shape", "preserve").build());

        return builder.build();
    }
}
