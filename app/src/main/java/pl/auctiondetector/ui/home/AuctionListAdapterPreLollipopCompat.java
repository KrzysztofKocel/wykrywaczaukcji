package pl.auctiondetector.ui.home;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v7.graphics.Palette;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.squareup.picasso.RequestCreator;

import pl.auctiondetector.utils.CompatUtils;

public class AuctionListAdapterPreLollipopCompat extends AuctionListAdapter {

    private int mCornerRadius;

    @Override
    protected RequestCreator getPicassoRequestCreator(Uri auctionImageLink, ImageView imageView) {
        // TODO read corner radius from xml ??
        mCornerRadius = dpToPx(imageView.getContext(), 4);
        return super.getPicassoRequestCreator(auctionImageLink, imageView).transform(RoundedTransformation.getInstance(mCornerRadius, 0));
    }

    @Override
    protected void setTitleBackground(Palette.Swatch vibrantSwatch, ViewHolder holder) {
        GradientDrawable roundedBackground = new GradientDrawable();
        roundedBackground.setCornerRadii(new float[]{0, 0, 0, 0, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius});
        roundedBackground.setColor(vibrantSwatch.getRgb() & 0xefffffff);
        CompatUtils.setViewBackground(holder.mTextView, roundedBackground);
    }

    private int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
