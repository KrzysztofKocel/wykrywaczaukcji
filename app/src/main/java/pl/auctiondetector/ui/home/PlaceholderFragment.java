package pl.auctiondetector.ui.home;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.collect.ImmutableMap;

import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import java.util.Collection;

import pl.auctiondetector.R;
import pl.auctiondetector.model.Auction;
import pl.auctiondetector.rss.service.RssService;
import retrofit.RestAdapter;
import retrofit.converter.SimpleXMLConverter;

import static pl.auctiondetector.rss.service.RssService.URL;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AuctionListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.auctions_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mAdapter = new AuctionListAdapterPreLollipopCompat();
        } else {
            mAdapter = new AuctionListAdapter();
        }


        mRecyclerView.setAdapter(mAdapter);

        // TODO move it to sync adapter
        new AsyncTask<Void, Void, Collection<Auction>>() {
            @Override
            protected void onPostExecute(Collection<Auction> auctions) {
                super.onPostExecute(auctions);

                mAdapter.setData(auctions);
            }

            @Override
            protected Collection<Auction> doInBackground(Void... params) {

                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(URL)
                        .setConverter(new SimpleXMLConverter(new Persister(new AnnotationStrategy())))
                        .build();

                return restAdapter.create(RssService.class).getAuctionsRss(
                        ImmutableMap.of(
                                RssService.Query.STRING, "hub",
                                RssService.Query.SEARCH_TYPE, "1")).getAuctions();
            }
        }.execute();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
