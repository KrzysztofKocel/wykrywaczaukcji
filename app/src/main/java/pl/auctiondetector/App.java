package pl.auctiondetector;

import android.app.Application;
import android.content.Context;


public class App extends Application {
    AppComponent mAppComponent;

    public static App from(Context context) {
        return ((App) context.getApplicationContext());
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = Dagger_AppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent component() {
        return mAppComponent;
    }
}
